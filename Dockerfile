FROM python:3.6

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY server.py /usr/src/app

EXPOSE 8083

# CMD ["python3", "-m",  "http.server", "8080"]
CMD ["python3", "./server.py"]
